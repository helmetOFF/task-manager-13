package ru.helmetoff.tm.api.controller;

import ru.helmetoff.tm.model.Task;

public interface ITaskController {

    void showTasks();

    void showTask(Task task);

    void clearTasks();

    void createTask();

    void removeOneByIndex();

    void removeOneById();

    void removeOneByName();

    void viewOneByIndex();

    void viewOneById();

    void viewOneByName();

    void updateOneByIndex();

    void updateOneById();

}
