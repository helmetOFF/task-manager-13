package ru.helmetoff.tm.api.repository;

import ru.helmetoff.tm.model.Command;

public interface ICommandRepository {

    String[] getCommands(Command... values);

    String[] getArguments(Command... values);

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();

}
