package ru.helmetoff.tm.api.repository;

import ru.helmetoff.tm.model.Project;
import ru.helmetoff.tm.model.Task;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findOneByIndex(Integer index);

    Project findOneById(String id);

    Project findOneByName(String name);

    void removeProjectByIndex(Integer index);

    void removeProjectById(String id);

    void removeProjectByName(String name);

}
