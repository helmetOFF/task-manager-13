package ru.helmetoff.tm.api.service;

import ru.helmetoff.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();

}
