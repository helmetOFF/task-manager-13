package ru.helmetoff.tm.api.service;

import ru.helmetoff.tm.model.Project;
import ru.helmetoff.tm.model.Task;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    void removeOneById(String id);

    void removeOneByIndex(Integer index);

    void removeOneByName(String name);

    Project updateOneById(String id, String name, String description);

    Project updateOneByIndex(Integer index, String name, String description);

}
