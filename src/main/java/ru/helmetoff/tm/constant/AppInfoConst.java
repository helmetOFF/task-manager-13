package ru.helmetoff.tm.constant;

public interface AppInfoConst {

    String VERSION = "0.13.0";

    String DEVELOPER_NAME = "Vladislav Halmetov";

    String DEVELOPER_EMAIL = "halmetoff@gmail.com";

}
