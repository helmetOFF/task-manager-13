package ru.helmetoff.tm.controller;

import ru.helmetoff.tm.api.controller.IProjectController;
import ru.helmetoff.tm.api.service.IProjectService;
import ru.helmetoff.tm.model.Project;
import ru.helmetoff.tm.model.Task;
import ru.helmetoff.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECTS LIST]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showProject(Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void removeOneByIndex() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        projectService.removeOneByIndex(index);
        System.out.println("[OK]");
    }

    @Override
    public void removeOneById() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        projectService.removeOneById(id);
        System.out.println("[OK]");
    }

    @Override
    public void removeOneByName() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        projectService.removeOneByName(name);
        System.out.println("[OK]");
    }

    @Override
    public void viewOneByIndex() {
        System.out.println("[VIEW PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void viewOneById() {
        System.out.println("[VIEW PROJECT]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void viewOneByName() {
        System.out.println("[VIEW PROJECT]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void updateOneByIndex() {
        System.out.println("[VIEW PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateOneByIndex(index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateOneById() {
        System.out.println("[VIEW TASK]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateOneById(id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }
}
