package ru.helmetoff.tm.error;

public class CommandIncorrectException extends RuntimeException {

    public CommandIncorrectException() {
        super("Error! Command is incorrect...");
    }

    public CommandIncorrectException(final String command) {
        super("Error! This command \"" + command + "\" is incorrect...");
    }
}
