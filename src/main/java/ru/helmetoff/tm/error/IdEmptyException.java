package ru.helmetoff.tm.error;

public class IdEmptyException extends RuntimeException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
