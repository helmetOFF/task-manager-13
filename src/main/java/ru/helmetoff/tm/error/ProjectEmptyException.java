package ru.helmetoff.tm.error;

public class ProjectEmptyException extends RuntimeException {

    public ProjectEmptyException() {
        super("Error! Project is empty...");
    }

}
