package ru.helmetoff.tm.error;

public class ProjectNotFoundException extends RuntimeException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
