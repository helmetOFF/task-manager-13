package ru.helmetoff.tm.error;

public class TaskEmptyException extends RuntimeException {

    public TaskEmptyException() {
        super("Error! Task is empty...");
    }

}
