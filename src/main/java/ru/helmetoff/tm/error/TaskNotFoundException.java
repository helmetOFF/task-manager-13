package ru.helmetoff.tm.error;

public class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
