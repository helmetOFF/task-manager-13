package ru.helmetoff.tm.repository;

import ru.helmetoff.tm.api.repository.IProjectRepository;
import ru.helmetoff.tm.model.Project;
import ru.helmetoff.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByName(final String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public void removeProjectByIndex(final Integer index) {
        projects.remove(index.intValue());
    }

    @Override
    public void removeProjectById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return;
        remove(project);
    }

    @Override
    public void removeProjectByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) return;
        remove(project);
    }
}
