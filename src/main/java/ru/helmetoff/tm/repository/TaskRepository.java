package ru.helmetoff.tm.repository;

import ru.helmetoff.tm.api.repository.ITaskRepository;
import ru.helmetoff.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task : tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public void removeTaskByIndex(final Integer index) {
        tasks.remove(index.intValue());
    }

    @Override
    public void removeTaskById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return;
        remove(task);
    }

    @Override
    public void removeTaskByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return;
        remove(task);
    }
}
