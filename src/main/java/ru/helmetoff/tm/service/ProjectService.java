package ru.helmetoff.tm.service;

import ru.helmetoff.tm.api.repository.IProjectRepository;
import ru.helmetoff.tm.api.service.IProjectService;
import ru.helmetoff.tm.error.*;
import ru.helmetoff.tm.model.Project;
import ru.helmetoff.tm.model.Task;

import java.util.List;

public class ProjectService implements IProjectService {

    private IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) throw new ProjectEmptyException();
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) throw new ProjectEmptyException();
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return projectRepository.findOneByName(name);
    }

    @Override
    public void removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        projectRepository.removeProjectById(id);
    }

    @Override
    public void removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        projectRepository.removeProjectByIndex(index);
    }

    @Override
    public void removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        projectRepository.removeProjectByName(name);
    }

    @Override
    public Project updateOneById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateOneByIndex(final Integer index, final String name, final String description) {
        if (index == null || index.intValue() < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }
}
