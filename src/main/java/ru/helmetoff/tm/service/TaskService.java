package ru.helmetoff.tm.service;

import ru.helmetoff.tm.api.repository.ITaskRepository;
import ru.helmetoff.tm.api.service.ITaskService;
import ru.helmetoff.tm.error.*;
import ru.helmetoff.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(final Task task) {
        if (task == null) throw new TaskEmptyException();
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) throw new TaskEmptyException();
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public void removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        taskRepository.removeTaskById(id);
    }

    @Override
    public void removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        taskRepository.removeTaskByIndex(index);
    }

    @Override
    public void removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        taskRepository.removeTaskByName(name);
    }

    @Override
    public Task updateOneById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateOneByIndex(final Integer index, final String name, final String description) {
        if (index == null || index.intValue() < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }
}
